<?php

require 'librerias/phpword/vendor/autoload.php';

// Cargando el documento plantilla...
$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('plantillas/documento_plantilla.docx');

//Reemplazando las variables de la plantilla por los valores que nosotros deseemos
$templateProcessor->setValue('variable1', 'Esta es la variable 1');
$templateProcessor->setValue('variable2', 'Esta es la variable 2');

$templateProcessor->setValue('nombre', 'Edgar');
$templateProcessor->setValue('apellidos', 'Bautista');

$templateProcessor->setValue('telefono', '771111111');
$templateProcessor->setValue('email', 'edgar@mail.com');

$variables_tabla=[['cantidad' => 5,'unidad_medida'=>'Litro','descripcion'=>'Agua','costo'=> '$'.number_format(150,2)],
    ['cantidad' => 3,'unidad_medida'=>'Pieza','descripcion'=>'Pan','costo'=> '$'.number_format(15,2)]];

$templateProcessor->cloneRowAndSetValues('cantidad', $variables_tabla);

//Preparando para descargar el docuemnto
header('Content-Disposition: attachment; filename="Documento.docx"');
//Enviar el output del objeto al explorador para descarga del usuario.
$templateProcessor->saveAs("php://output");
?>