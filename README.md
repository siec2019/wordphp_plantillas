# README #

Proyecto CultusDevelopers de la Dirección de Innovación y Emprendimiento Cultural de la Secretaría de Cultura del Estado de Hidalgo 

### ¿Para qué sirve este repositorio? ###

* El presente repositorio pertenece a la iniciativa CultusDevelopers dedicada a difundir las herramientas utilizadas por la Dirección de Innovación y Emprendimiento Cultural, el principal propósito es el de contribuir a los desarrolladores del sector público y privado de herramientas que puedan ser útiles en la implementación de sus proyectos.
* El presente proyecto intenta ayudar a la implementación del proyecto de la librería PHPWord y uno de sus módulos más útiles como es el trabajar con plantillas y variables dentro de documentos de WORD.
* Versión del proyecto 1.0
* Link del proyecto PHPWord(https://github.com/PHPOffice/PHPWord)

### ¿Como puedo instalarlo? ###

* El presente proyecto está listo para ser desplegado de inmediato, contiene la librería implementada y un ejemplo funcional.
* En caso de necesitar más ayuda y explorar la librería más a fondo puede visitar la página oficial: https://phpword.readthedocs.io/en/latest/intro.html

### Notas del código ###

* El código está escrito en PHP para la generación de documentos Microsoft Office Open XML
* Para más información de la implementación y el código puede leer el manual adjunto en /documentos/Llenar variables en plantillas de Word utilizando PHP.pdf

### Contacto ###

* Correo: siec.sistemas@hidalgo.gob.mx
